"""
    API-функции для работы с сущностью User для позитивных сценариев
"""
from http import HTTPStatus

from src.entity.api_entity.user.api_func import UserApiFunc


class UserApiFuncPositive:

    @staticmethod
    def create(user_body, **kwargs):
        """Создаем User-а"""
        response = UserApiFunc.create(user_body, **kwargs)
        assert response.status_code == HTTPStatus.OK

    @staticmethod
    def get(user_name, **kwargs):
        """Получаем User-а"""
        response = UserApiFunc.get(user_name, **kwargs)
        assert response.status_code == HTTPStatus.OK
        return response.json()

    @staticmethod
    def update(user_name, user_body, **kwargs):
        """Обновляем User-а"""
        response = UserApiFunc.update(user_name, user_body, **kwargs)
        assert response.status_code == HTTPStatus.OK

    @staticmethod
    def delete(user_name, **kwargs):
        UserApiFunc.delete(user_name, **kwargs)
