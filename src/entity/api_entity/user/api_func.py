"""
    API-функции для работы с сущностью User
"""
from src.clients.api import ApiClient
from src.entity.api_entity.user.api_path import UserFullPath


class UserApiFunc:

    @staticmethod
    def create(user_body, **kwargs):
        """Создаем User-а"""
        return ApiClient.post(url=UserFullPath.create, json=user_body, **kwargs)

    @staticmethod
    def get(user_name, **kwargs):
        """Получаем User-а"""
        return ApiClient.get(url=UserFullPath.get.value / user_name, **kwargs)

    @staticmethod
    def update(user_name: str, user_body: dict, **kwargs):
        """Обновляем User-а"""
        return ApiClient.put(url=UserFullPath.put.value / user_name, json=user_body, **kwargs)

    @staticmethod
    def delete(user_name: str, **kwargs):
        return ApiClient.delete(url=UserFullPath.delete.value / user_name, **kwargs)


if __name__ == "__main__":
    user_body = {
        "username": "iivanov2",
        "firstName": "Ivan2",
        "lastName": "Ivanov2",
        "email": "iivanov@mail2.ru",
        "password": "1234562",
        "phone": "+79000000002"
    }
    create_response = UserApiFunc.create(user_body)
    _ = create_response.json()
    get_response = UserApiFunc.get(user_body["username"])
    _ = get_response.json()
    user_update_body = {
        "firstName": "Nick"
    }
    update_response = UserApiFunc.update(user_body["username"], user_update_body)
    _ = update_response.json()
    get_response = UserApiFunc.get(user_body["username"])
    _ = get_response.json()
    pass
