"""
    API-пути для данной сущности
"""
from enum import Enum
from strenum import StrEnum

from src.config import base_url
from src.entity.api_entity.api_path import VersionNumber
from src.entity.api_entity.user import entity_name


class UserPath(StrEnum):
    """Локальные пути"""
    create = f"{entity_name}"
    createWithArray = f"{entity_name}/createWithArray"
    createWithList = f"{entity_name}/createWithList"
    get = f"{entity_name}"
    put = f"{entity_name}"
    delete = f"{entity_name}"
    login = f"{entity_name}/login"
    logout = f"{entity_name}/logout"


class UserFullPath(Enum):
    """Полные пути"""
    create = base_url / VersionNumber.v2 / UserPath.create
    createWithArray = base_url / VersionNumber.v2 / UserPath.createWithArray
    createWithList = base_url / VersionNumber.v2 / UserPath.createWithList
    get = base_url / VersionNumber.v2 / UserPath.get
    put = base_url / VersionNumber.v2 / UserPath.put
    delete = base_url / VersionNumber.v2 / UserPath.delete
    login = base_url / VersionNumber.v2 / UserPath.login
    logout = base_url / VersionNumber.v2 / UserPath.logout


if __name__ == "__main__":
    print(UserFullPath.create.value)
    print(UserFullPath.createWithArray.value)
    print(UserFullPath.createWithList.value)
    print(UserFullPath.get.value)
    print(UserFullPath.put.value)
    print(UserFullPath.delete.value)
    print(UserFullPath.login.value)
    print(UserFullPath.logout.value)
