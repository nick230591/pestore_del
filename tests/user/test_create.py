"""API тесты для create URL"""

import pytest

from src.entity.api_entity.user.api_func_positive import UserApiFuncPositive
from tests.config import faker_ru


@pytest.mark.positive
@pytest.mark.user
def test_create_user_success():
    user_body = {
        "username": faker_ru.user_name(),
        "firstName": faker_ru.first_name(),
        "lastName": faker_ru.last_name(),
        "email": faker_ru.email(),
        "password": faker_ru.password(),
        "phone": faker_ru.phone_number(),
    }
    UserApiFuncPositive.create(user_body)
